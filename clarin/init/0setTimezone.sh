#!/bin/sh

# Set up correct timezone
apk add tzdata
TZ=Europe/Amsterdam
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone