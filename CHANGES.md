# version 3.0.0
- giving up log volumes since logs are persisted by fluentd anyway 
- adding new container for linkchecker-web

# version 2.0.0
- new start script

# version 1.9.6
- adding functionality to backup/restore linkchecker database
    - mounting a directory for backups in docker-compose.yml
    - adding scripts backup.sh and restore.sh

# version 1.9.5
- image upgrade for [linkchecker 2.1.2](https://gitlab.com/CLARIN-ERIC/docker-linkchecker/-/blob/master/CHANGES.md) (in docker-compose.yml)
- mounting zookeeper and storm log directories to docker volume (in docker-compose.yml)
- deploying linkchecker topology with first call of 'start-link-checking' in active state, deactivating and activating it with any further call of 'stop-link-checking/start-link-checking' (in custom.sh) 