#!/bin/bash
# author: Wolfgang Walter SAUER (wowasa)

VERBOSE=0

#
# Process script arguments and parse all flags
#
for i in "$@"
do
case $i in
    -v|--verbose)
        VERBOSE=1
        ;;
esac
#shift # past argument or value
done

if [ "${COMPOSE_DIR}" == "" ]; then
    echo "Backup script compose directory not set (COMPOSE_DIR)"
    exit 1
fi
if [ "${BACKUPS_DIR}" == "" ]; then
    echo "Backup script backups directory not set (BACKUPS_DIR)"
    exit 1
fi
if [ "${BACKUPS_TIMESTAMP}" == "" ]; then
    echo "Backup timestamps not set (BACKUPS_TIMESTAMP)"
    exit 1
fi

START_DIR="$(pwd)"
(

	#
	# Script backup / restore functionality
	# Restore the file ${RESTORE_FILE} of implement custom logic.
	#
	export BACKUPS_DIR="$(realpath "${BACKUPS_DIR}")"

	MYSQL_OPTS=()
	if [ "${VERBOSE}" == '1' ]; then
		MYSQL_OPTS+="-v"
		echo "pwd: $(pwd)"
		echo "COMPOSE_DIR: ${COMPOSE_DIR}"
		echo "BACKUPS_DIR: ${BACKUPS_DIR}"
		echo "BACKUP_FILE_NAME: ${BACKUP_FILE_NAME}"
		echo "MYSQL_OPTS: ${MYSQL_OPTS}"
	fi

	if [ "${VERBOSE}" == '1' ]; then
		set -x
	fi

	(
		cd "${COMPOSE_DIR}/clarin" &&
		docker-compose exec -T mysql \
			nice bash -c \
				'mysqldump '"${MYSQL_OPTS}"' -u ${MYSQL_USER} --password=${MYSQL_PASSWORD} ${MYSQL_DATABASE}|gzip > /backups/backup-${MYSQL_DATABASE}_'"${BACKUPS_TIMESTAMP}"'.sql.gz'
	)
)
STATUS=$?
cd "${START_DIR}"
exit "${STATUS}"
