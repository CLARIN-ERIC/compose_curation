#!/bin/bash
# author: Wolfgang Walter SAUER (wowasa)

VERBOSE=0

#
# Process script arguments and parse all flags
#
for i in "$@"
do
case $i in
    -v|--verbose)
        VERBOSE=1
        ;;
esac
#shift # past argument or value
done

if [ "${COMPOSE_DIR}" == "" ]; then
    echo "Backup script compose directory not set (COMPOSE_DIR)"
    exit 1
fi
if [ "${RESTORE_FILE}" == "" ]; then
    echo "Restore file not set (RESTORE_FILE)"
    exit 1
fi


START_DIR="$(pwd)"
(
	cd "${COMPOSE_DIR}/.."
	
	MYSQL_OPTS=()
	if [ "${VERBOSE}" == '1' ]; then
		MYSQL_OPTS+="-v"
		echo "PWD: $(pwd)"
		echo "RESTORE_FILE: ${RESTORE_FILE} (=$(realpath ${RESTORE_FILE}))"
		echo "MYSQL_OPTS: ${MYSQL_OPTS}"
	fi

	if [ -e "${RESTORE_FILE}" ]; then
		RESTORE_FILE="$(realpath "${RESTORE_FILE}")"
		if [ "${VERBOSE}" == '1' ]; then
			echo "Restore file found! ${RESTORE_FILE}"
		fi
	else
		echo "Error: restore file not found ${RESTORE_FILE}"
		cd "${START_DIR}"
		exit 1
	fi
	
	RESTORE_FILE_BASENAME="$(basename ${RESTORE_FILE})"
	if [ "${VERBOSE}" == '1' ]; then
		echo "RESTORE_FILE_BASENAME: ${RESTORE_FILE_BASENAME}"
	fi

	if [ "${VERBOSE}" == '1' ]; then
		set -x
	fi

	(
		cd "${COMPOSE_DIR}/clarin" &&
		docker-compose exec -T mysql \
			nice bash -c \
				'gunzip -c /backups/'"${RESTORE_FILE_BASENAME}"'|mysql '"${MYSQL_OPTS}"' -u ${MYSQL_USER} --password=${MYSQL_PASSWORD} ${MYSQL_DATABASE}'
	)
)
STATUS=$?
cd "${START_DIR}"
exit "${STATUS}"
